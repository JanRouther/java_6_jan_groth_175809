package net.stawrul.services.exceptions;

/**
 * Wyjątek sygnalizujący za niska wartosc zamowienia
 *
 * Wystąpienie wyjątku z hierarchii RuntimeException w warstwie biznesowej
 * powoduje wycofanie transakcji (rollback).
 */
public class CheapOrderException extends RuntimeException {
}
