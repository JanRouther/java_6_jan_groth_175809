package net.stawrul.model;
import lombok.Getter;
import lombok.Setter;
import lombok.EqualsAndHashCode;


import javax.persistence.*;
import java.util.UUID;




/**
 * Klasa encyjna reprezentująca zamówienie.
 */
@Entity
@Table(name = "orderedbooks")
@EqualsAndHashCode(of = "id")
public class OrderedBooks {

    @Getter
    @Setter
    @Id
    UUID id = UUID.randomUUID();

    @Getter
    @Setter
    @ManyToOne
    Book book;

    @Getter
    @Setter
    Integer amount;


}
