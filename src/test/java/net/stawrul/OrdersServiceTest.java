package net.stawrul;


import net.stawrul.model.*;
import net.stawrul.services.*;
import net.stawrul.services.exceptions.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import javax.persistence.EntityManager;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
public class OrdersServiceTest {
    @Mock
    private EntityManager em;

    @Test
    public void whenOrderedBookAvailable_placeOrderDecreasesAmountByOne() {
        //Arrange
        Order order = new Order();//order lista ksiazek w zamowieniu
        Book book = new Book();
        OrderedBooks ordered =new OrderedBooks();

        book.setAmount(1);
        book.setPrice(200);
        ordered.setBook(book);
        ordered.setAmount(1);
        order.getBooksorders().add(ordered);

        Mockito.when(em.find(Book.class, book.getId())).thenReturn(book);

        OrdersService ordersService = new OrdersService(em);

        //Act
        ordersService.placeOrder(order);

        //Assert
        //dostępna liczba książek zmniejszyła się:
        assertEquals(0, (int)book.getAmount());
        //nastąpiło dokładnie jedno wywołanie em.persist(order) w celu zapisania zamówienia:
        Mockito.verify(em, times(1)).persist(order);
    }


    @Test(expected = ManyBooksOrderedException.class)
    public void whenTooManyItemsOrdered_placeOrderManyBooksOrderedException(){
        //Arrange
        Order order = new Order();//order lista ksiazek w zamowieniu
        Book book = new Book();
        OrderedBooks ordered =new OrderedBooks();

        book.setAmount(100);
        book.setPrice(200);
        ordered.setBook(book);
        ordered.setAmount(80);
        order.getBooksorders().add(ordered);

        Book book2 = new Book();
        OrderedBooks ordered2 =new OrderedBooks();

        book2.setAmount(150);
        book2.setPrice(250);
        ordered2.setBook(book2);
        ordered2.setAmount(70);
        order.getBooksorders().add(ordered2);

        Mockito.when(em.find(Book.class, book.getId())).thenReturn(book);
        Mockito.when(em.find(Book.class, book2.getId())).thenReturn(book2);

        OrdersService ordersService = new OrdersService(em);

        //Act
        ordersService.placeOrder(order);
        //Assert ManyBooksOrderedException expected
    }

    @Test(expected = OutOfStockException.class)
    public void whenThereIsNotEnoughBooksInStock_placeOrderOutOfStockException(){
        //Arrange
        Order order = new Order();//order lista ksiazek w zamowieniu
        Book book = new Book();
        OrderedBooks ordered =new OrderedBooks();

        book.setAmount(30);
        book.setPrice(200);
        ordered.setBook(book);
        ordered.setAmount(8);
        order.getBooksorders().add(ordered);

        Book book2 = new Book();
        OrderedBooks ordered2 =new OrderedBooks();

        book2.setAmount(22);
        book2.setPrice(250);
        ordered2.setBook(book2);
        ordered2.setAmount(5);
        order.getBooksorders().add(ordered2);


        Book book3 = new Book();
        OrderedBooks ordered3 =new OrderedBooks();

        book3.setAmount(5);
        book3.setPrice(144);
        ordered3.setBook(book3);
        ordered3.setAmount(12);
        order.getBooksorders().add(ordered3);

        Mockito.when(em.find(Book.class, book.getId())).thenReturn(book);
        Mockito.when(em.find(Book.class, book2.getId())).thenReturn(book2);
        Mockito.when(em.find(Book.class, book3.getId())).thenReturn(book3);

        OrdersService ordersService = new OrdersService(em);

        //Act
        ordersService.placeOrder(order);
        //Assert OutOfStockException expected

    }

    @Test(expected =CheapOrderException.class)
    public void whenOrderIsWorthSmallAmount_placeCheapOrderException(){
        //Arrange
        Order order = new Order();//order lista ksiazek w zamowieniu
        Book book = new Book();
        OrderedBooks ordered =new OrderedBooks();

        book.setAmount(123);
        book.setPrice(2);
        ordered.setBook(book);
        ordered.setAmount(10);
        order.getBooksorders().add(ordered);

        Book book2 = new Book();
        OrderedBooks ordered2 =new OrderedBooks();

        book2.setAmount(50);
        book2.setPrice(3);
        ordered2.setBook(book2);
        ordered2.setAmount(3);
        order.getBooksorders().add(ordered2);

        Mockito.when(em.find(Book.class, book.getId())).thenReturn(book);
        Mockito.when(em.find(Book.class, book2.getId())).thenReturn(book2);

        OrdersService ordersService = new OrdersService(em);

        //Act
        ordersService.placeOrder(order);
        //Assert CheapOrderExceptionexpected
    }

    @Test
    public void whenManyBookAvailable_placeOrderDecreasesAmountByCorrectValues() {
        //Arrange
        Order order = new Order();//order lista ksiazek w zamowieniu
        Book book = new Book();
        OrderedBooks ordered =new OrderedBooks();

        book.setAmount(344);
        book.setPrice(200);
        ordered.setBook(book);
        ordered.setAmount(4);
        order.getBooksorders().add(ordered);

        Book book2 = new Book();
        OrderedBooks ordered2 =new OrderedBooks();

        book2.setAmount(225);
        book2.setPrice(250);
        ordered2.setBook(book2);
        ordered2.setAmount(5);
        order.getBooksorders().add(ordered2);


        Book book3 = new Book();
        OrderedBooks ordered3 =new OrderedBooks();

        book3.setAmount(564);
        book3.setPrice(144);
        ordered3.setBook(book3);
        ordered3.setAmount(12);
        order.getBooksorders().add(ordered3);

        Book book4= new Book();
        OrderedBooks ordered4 =new OrderedBooks();

        book4.setAmount(3);
        book4.setPrice(124);
        ordered4.setBook(book4);
        ordered4.setAmount(3);
        order.getBooksorders().add(ordered4);

        Mockito.when(em.find(Book.class, book.getId())).thenReturn(book);
        Mockito.when(em.find(Book.class, book2.getId())).thenReturn(book2);
        Mockito.when(em.find(Book.class, book3.getId())).thenReturn(book3);
        Mockito.when(em.find(Book.class, book4.getId())).thenReturn(book4);

        OrdersService ordersService = new OrdersService(em);

        //Act
        ordersService.placeOrder(order);
        //Assert
        assertEquals(340, (int)book.getAmount());
        assertEquals(220, (int)book2.getAmount());
        assertEquals(552, (int)book3.getAmount());
        assertEquals(0, (int)book4.getAmount());
        //nastąpiło dokładnie jedno wywołanie em.persist(order) w celu zapisania zamówienia:
        Mockito.verify(em, times(1)).persist(order);

    }
}







